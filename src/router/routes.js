export default [
	{
		path: '/',
		name: 'Variety',
		component: () => import('layouts/default'),
		children: [
			{
				name: 'KShow Online',
				path: 'kshow-online',
				component: () => import('pages/KShowOnline')
			},
			{
				name: 'kshow123',
				path: 'kshow123',
				component: () => import('pages/KShow123')
			}
		]
	},
	{
		path: '/drama',
		name: 'Drama',
		component: () => import('layouts/default'),
		children: [
			{
				name: 'dramago',
				path: 'dramago',
				component: () => import('pages/Dramago')
			}
		]
	},
	{
		// Always leave this as last one
		path: '*',
		component: () => import('pages/404')
	}
]
