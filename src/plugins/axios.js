import axios from 'axios'

export default ({ Vue }) => {
	Vue.prototype.$axios = axios.create({
		baseURL:
			window.location.hostname === 'localhost'
				? 'http://localhost:3000/apis/'
				: window.location.origin + '/apis/'
	})
}
